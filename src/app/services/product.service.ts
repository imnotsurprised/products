import {Injectable} from "@angular/core";
import { Product } from "../shared/product";
import { PRODUCTS_LIST } from "../shared/data";

@Injectable()
export class ProductService {

    private products: Product[] = PRODUCTS_LIST;

    getProducts(): Product[] {
        return this.products;
    }

    removeProduct(index: number): void {
        this.products.splice(index, 1);
    }

    getProductById(id: number): Product {
        const product = this.products.filter(product => {
            return product.id == id;
        });

        return product[0];
    }

    saveProduct(product: Product): boolean {
        if (product.id == null) {
            product.id = this.getAvailableId();
            this.products.push(product);
        }
        else {
            let currentProduct = this.getProductById(product.id);
            currentProduct.name = product.name;
            currentProduct.description = product.description;
            currentProduct.price = product.price;
        }

        return true;
    }

    getAvailableId(): number {
        if (!this.products.length) {
            return 1;
        }

        let product = this.products[this.products.length -1];

        return product.id +1;
    }
}