import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { Product } from '../../shared/product';
import { ProductService } from "../../services/product.service";

@Component({
    templateUrl: './product-edit.component.html'
})
export class ProductEditComponent implements OnInit {
    id: number = null;
    product: Product = null;

    constructor(private route:ActivatedRoute, private productService:ProductService) { }

    ngOnInit() {
        this.route.params.subscribe( params => {
            this.id = +params['id'];
        });

        this.product = this.productService.getProductById(this.id);
    }
}