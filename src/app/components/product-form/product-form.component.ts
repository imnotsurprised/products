import {Component, OnChanges, Input} from '@angular/core';
import { Router } from '@angular/router';

import { Product } from '../../shared/product';
import { ProductService } from "../../services/product.service";

@Component({
    selector: 'product-form',
    templateUrl: './product-form.component.html'
})
export class ProductFormComponent implements OnChanges {
    @Input() product: Product;
    model: {
        id: number;
        name: string;
        description: string;
        price: number;
    } = {
        id: null,
        name: '',
        description: '',
        price: null
    };

    constructor(private productService:ProductService, private router:Router) { }

    ngOnChanges() {
        if (this.product) {
            this.model.id = this.product.id;
            this.model.name = this.product.name;
            this.model.description = this.product.description;
            this.model.price = this.product.price;
        }
    }

    save() {
        let record: Product = {
            id: null,
            name: this.model.name,
            description: this.model.description,
            price: this.model.price
        };

        if (this.product) {
            record.id = this.product.id;
        }

       this.productService.saveProduct(record);
       this.router.navigate(['']);
    }
}