import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

import { Product } from '../../shared/product';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html'
})
export class ProductListComponent implements OnInit {

    public data: Product[] = null;
    public rowsOnPage: number = 5;

    constructor(private productService: ProductService, private router: Router) {}

    addProduct() {
        this.router.navigate(['/new']);
    }

    ngOnInit(): void {
        this.data = this.productService.getProducts();
    }

    removeProduct(id: number): void {
        this.productService.removeProduct(id);
    }
}