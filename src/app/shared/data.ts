import { Product } from './product';

export const PRODUCTS_LIST: Product[] = [
    {
        "id": 1,
        "price": 45.12,
        "name": "TV Stand",
        "description": ""
    },
    { 
        "id": 2,
        "price": 25.12,
        "name": "BBQ Grill",
         "description": "" 
    },
    { 
        "id": 3,
        "price": 43.12,
        "name": "Magic Carpet",
        "description": ""
    },
    {
        "id": 4,
        "price": 12.12,
        "name": "Instant liquidifier",
        "description": ""
    },
    {
        "id": 5,
        "price": 9.12,
        "name": "Box of puppies",
        "description": ""
    },
    { 
        "id": 6,
        "price": 7.34,
        "name": "Laptop Desk",
        "description": ""
    },
    {
        "id": 7,
        "price": 5.34,
        "name": "Water Heater",
        "description": ""
    },
    {
        "id": 8,
        "price": 4.34,
        "name": "Smart Microwave",
        "description": ""
    },
    {
        "id": 9,
        "price": 93.34,
        "name": "Circus Elephant",
        "description": ""
    },
    { 
        "id": 10,
        "price": 87.34,
        "name": "Tinted Window",
        "description": ""
    }
];