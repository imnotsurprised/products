import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ProductNewComponent } from './components/product-new/product-new.component';
import { ProductEditComponent } from './components/product-edit/product-edit.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'new', component: ProductNewComponent },
    { path: 'edit/:id', component: ProductEditComponent },
    { path: 'view/:id', component: ProductDetailsComponent },
    { path: '**', redirectTo: '' }
];

export const routing  = RouterModule.forRoot(routes, { useHash: true });