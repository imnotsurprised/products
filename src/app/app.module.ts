import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DataTableModule } from "angular2-datatable";

import { AppComponent } from './components/app/app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { ProductListComponent } from './components/product-list/product-list.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductEditComponent } from './components/product-edit/product-edit.component';
import { ProductNewComponent } from './components/product-new/product-new.component';

import { ProductService } from './services/product.service';

import { routing } from './app.routes';

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        ProductFormComponent,
        ProductListComponent,
        ProductDetailsComponent,
        ProductEditComponent,
        HeaderComponent,
        ProductNewComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        DataTableModule,
        routing
    ],
    providers: [ProductService],
    bootstrap: [AppComponent]
})
export class AppModule { }